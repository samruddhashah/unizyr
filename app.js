const mongoose = require("mongoose");
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const userroutes = require("./routes/user")

//connect to db
mongoose
    .connect('mongodb://localhost:27017/test1', 
        {
            useNewUrlParser: true, 
            useUnifiedTopology: true,
            useCreateIndex:true
            
    })
    .then(()=>{
        console.log("db connected")
    });


app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors());
    


//my routes
app.use("/api",userroutes);

//port

var port ;

//push.port(4200)
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
  });
   
  readline.question('enter port?', enter => {
    port=enter;
    readline.close();
  });

//starting server 
setTimeout(function(){ app.listen(port, ()=>{
    console.log(`app is running at ${port}`); })}, 10000);
