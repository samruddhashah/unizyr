const User = require("../models/user") 


exports.enterdata = (req,res)=>{

    const user = new User(req.body);
    user.save((err, user)=>{
        if(err){
            return res.status(400).json({
                error:"not able to post"
            });

        }
        res.json(user)
    } 

)}


exports.getdata = (req,res)=>{
    User.find().exec((err,user)=>
    {
        if(err)
        {
            return res.status(400).json({
                error:"not found any data"
            })
        }
        res.json(user);
    })

}