var express = require('express');
var router = express.Router();
const {enterdata,getdata} = require("../controllers/user")

router.post("/enterdata",enterdata);

router.get("/getdata",getdata);

module.exports = router;