const mongoose = require('mongoose');

var userSchema = new mongoose.Schema({

    name: {
        type: String,
        required: true,
        maxlength: 32,
        trim: true
      },

    lastname: {
        type:String,
        required:true,
        maxlength: 32,
        trim: true
    },
    email: {
        type: String,
        required: true,
        maxlength:32,
        trim: true
    },
    mobile: {
        type: Number,
        required: true,
        maxlength: 10,
        trim: true
    },
    house:{
        type: String,
        maxlength: 10,
    },
    add1:{
        type: String,
        maxlength: 200
    },
    add:{
        type: String,
        maxlength: 200
    },
    city:{
        type:String
    },
    state:{
        type:String
    },
    pin:{
        type:Number,
        maxlength:6
    }
 
})

module.exports = mongoose.model("User",userSchema);




