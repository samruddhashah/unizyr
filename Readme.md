Simple api route to post and get user information.

1. Download files
2. Open in vs code
3. Install all dependencies via entering npm install
4. Run and enter port at which you want to start server
5. Install postman to use api.
6. Enter and edit your_port to get users http://localhost:your_port/api/getdata
7. Enter and edit your_port, use this format to post
{
"name":"sam",
"lastname":"shah",
"email":"samruddh@gmail.com",
"mobile":9923833365,
"house":"8/a",
"add1":"municipal colony",
"add":"wadibhokar road",
"city":"dhule",
"state":"mh",
"pin":424002
}

http://localhost:your_port/api/enterdata